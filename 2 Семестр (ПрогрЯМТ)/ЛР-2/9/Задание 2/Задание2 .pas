program laba_2_2_2;

var
  f:text;
  s: string;
  input: real;
  max, min: real;
  i: integer;
begin
  s: ReadAllText('input.txt');
  
  assign(f, 'input.txt');
  reset(f);
  
  while not eof(f) do
    begin
      inc(i);
      readln(f, input);
      if(input > max) then max:=input;
      if(input < min) then min:=input;
    end;
    
  close(f);
end.