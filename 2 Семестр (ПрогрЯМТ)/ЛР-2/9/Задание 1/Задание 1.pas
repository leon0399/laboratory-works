program laba_2_2_1;

var
  f, g: text;
  st: string;
  c: char; // ������� ��� ������ ������
begin
  read(c);

  assign(f, 'input.txt');
  reset(f);
  
  assign(g, 'output.txt');
  rewrite(g);
  
  while not eof(f) do
    begin
      readln(f, st);
      if(pos(c, st) <> 0) then
        begin
          delete(st, 1, pos(c, st));
          writeln(g, st, ' ', length(st));
        end;
    end;
    
  close(f);
  close(g);
end.