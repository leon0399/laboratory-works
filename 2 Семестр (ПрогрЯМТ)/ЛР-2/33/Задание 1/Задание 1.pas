program laba_2_2_1;

function findMaxByDigits(s: string): string; // ��������� �� ������ � ������� ��� �� ���-�� ����
var
  wrd: array[0..99] of string; // �����
  cnt: byte; // ���-�� ����
  digits: array[0..99] of integer; // ���-�� ���� ��� ������� �����
  i1, i2: integer; // ������� ��� �����
  max, maxI: integer; // ����. ��������
begin
  while (s<>'')and(pos(' ',s)<>0) do
    begin
      inc(cnt);
      wrd[cnt]:=copy(s,1,pos(' ',s)-1);
      delete(s,1,pos(' ',s));
    end;
    
  if s<>'' then
    begin
      inc(cnt);
      wrd[cnt]:=s;
    end;
    
  for i1:=0 to cnt do
    for i2:=1 to length(wrd[i1]) do
      if (wrd[i1][i2]>='0')and(wrd[i1][i2]<='9') then
        digits[i1]:=digits[i1]+1;
        
  maxI:=1;
  max:=digits[maxI];
  for i1:=1 to cnt do
    if(digits[i1] > max) then
      begin
        max:=digits[i1];
        maxI:=i1;
      end;
      
   findMaxByDigits:=wrd[maxI];
end;

var
  f, g: text; // ����� ��� ������
  st: string; // ������� ������
begin
  assign(f, 'input.txt');
  reset(f);
  
  assign(g, 'output.txt');
  rewrite(g);
  
  while not eof(f) do
    begin
      readln(f, st);
      writeln(g, findMaxByDigits(st));
    end;
    
  close(g);
  close(f);
end.