
const FILENAME = 'input.dat';

procedure init(s:string);
var
  n,i:integer;
  f:file of real;
begin
  assign(f,s);
  rewrite(f);
  write('���������� ����� = ');
  readln(n);
  for i:= 1 to n do
  begin
    write(f, (random(1000)-500) /10);
  end;
  close(f);
end;

procedure printFile(s:string);
var
  a: real;
  f:file of real;
begin
  assign(f, s);
  reset(f);
  
  while not eof(f) do
    begin
      read(f, a);
      writeln(a);
    end;
    
  close(f);
end;

procedure find(s:string);
var
  cur, max, sum: real;
  max_i, i: integer;
  f:file of real;
begin
  assign(f, s);
  reset(f);
  max := -1000;
  
  while not eof(f) do begin
    read(f, cur);
    inc(i);
    sum := sum + sqr(cur);
    if cur > max then begin
      max := cur;
      max_i := i;
    end;
  end;
    
  reset(f);
  seek(f, max_i);
  write(f, sum);
  
  close(f);
end;

begin
  init(FILENAME);
  println('��: ');
  printFile(FILENAME);
  
  find(FILENAME);
  println('�����: ');
  printFile(FILENAME);
end.