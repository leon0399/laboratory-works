const FILENAME = 'input.dat';

procedure init(s:string);
var
  n,i:integer;
  f:file of real;
begin
  assign(f,s);
  rewrite(f);
  write('���������� ����� = ');
  readln(n);
  for i:= 1 to n do
  begin
    write(f, (random(1000)-500) /10);
  end;
  close(f);
end;

procedure printFile(s:string);
var
  a: real;
  f:file of real;
begin
  assign(f, s);
  reset(f);
  
  while not eof(f) do
    begin
      read(f, a);
      writeln(a);
    end;
    
  close(f);
end;

procedure find(s:string);
var
  cur: real;
  mult: real = 1;
  f:file of real;
  n: integer;
begin
  assign(f, s);
  reset(f);
  
  while not eof(f) do
    begin
      read(f, cur);
      mult := mult * cur;
    end;
  
  writeln('N = ');
  readln(n);
  
  reset(f);
  seek(f, n);
  write(f, mult);
  
  close(f);
end;

begin
  init(FILENAME);
  println('��: ');
  printFile(FILENAME);
  
  find(FILENAME);
  println('�����: ');
  printFile(FILENAME);
end.