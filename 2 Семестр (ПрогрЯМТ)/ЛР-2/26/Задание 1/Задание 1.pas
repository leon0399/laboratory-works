program laba_2_2_1;

function isDigit(_word:string):boolean;
var
  i2: integer;
  flag: boolean = false;
begin
  if length(_word) > 0 then flag:= true;
  for i2:=1 to length(_word) do
    if not ((_word[i2]>='0') and (_word[i2]<='9')) then flag:=false;
    
  isDigit:=flag;
end;

procedure divideAndWrite(f,g:text);
var
  cur: string;
  cnt, i: integer;
  wrd: array[0..99] of string;
begin
  reset(f);
  rewrite(g);
  
  while not eof(f) do begin
    readln(f, cur);  
    
    cnt:=1;
    while (cur<>'')and(pos(' ',cur)<>0) do begin
      inc(cnt);
      wrd[cnt]:=copy(cur,1,pos(' ',cur)-1);
      delete(cur,1,pos(' ',cur));
    end;
    
    if cur<>'' then begin
      inc(cnt);
      wrd[cnt]:=cur;
    end;
    
    for i:=1 to cnt do if isDigit(wrd[i]) then writeln(g, wrd[i]);
  end;
  
  close(f);
  close(g);
end;

var
  f, g: text;
begin
  assign(f, 'input.txt');
  assign(g, 'output.txt');
  
  divideAndWrite(f, g);
end.