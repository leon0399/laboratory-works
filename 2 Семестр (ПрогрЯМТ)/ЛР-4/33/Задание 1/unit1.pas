unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TFormElectricity }

  TFormElectricity = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    Edit10: TEdit;
    Edit11: TEdit;
    Edit12: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    GroupMutualCapacity: TGroupBox;
    GroupPotentialDiff: TGroupBox;
    GroupCharge: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;

    RadioGroup1: TRadioGroup;
    RadioMutualCapacity: TRadioButton;
    RadioPotentialDiff: TRadioButton;
    RadioCharge: TRadioButton;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GroupPotentialDiffClick(Sender: TObject);
    procedure RadioChargeChange(Sender: TObject);
    procedure RadioMutualCapacityChange(Sender: TObject);
    procedure RadioPotentialDiffChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FormElectricity: TFormElectricity;

implementation

{$R *.lfm}

{ TFormElectricity }

procedure TFormElectricity.FormCreate(Sender: TObject);
begin
end;

procedure TFormElectricity.GroupPotentialDiffClick(Sender: TObject);
begin

end;

procedure TFormElectricity.RadioMutualCapacityChange(Sender: TObject);
begin
    GroupMutualCapacity.Visible:=true;
    GroupPotentialDiff.Visible:=false;
    GroupCharge.Visible:=false;
end;

procedure TFormElectricity.RadioPotentialDiffChange(Sender: TObject);
begin
    GroupMutualCapacity.Visible:=false;
    GroupPotentialDiff.Visible:=true;
    GroupCharge.Visible:=false;
end;

procedure TFormElectricity.RadioChargeChange(Sender: TObject);
begin
    GroupMutualCapacity.Visible:=false;
    GroupPotentialDiff.Visible:=false;
    GroupCharge.Visible:=true;
end;

procedure TFormElectricity.Button1Click(Sender: TObject);
begin
  if (Edit1.Text = '') or (Edit2.Text = '') or (Edit2.Text = '') then begin

  end else begin
      Edit4.Text := FloatToStr(StrToFloat(Edit1.Text) / (StrToFloat(Edit2.Text) - StrToFloat(Edit3.Text)));
  end;
end;

procedure TFormElectricity.Button2Click(Sender: TObject);
begin
  if (Edit6.Text = '') or (Edit7.Text = '') then begin

  end else begin
      Edit8.Text := FloatToStr((StrToFloat(Edit6.Text) - StrToFloat(Edit7.Text)));
  end;
end;

procedure TFormElectricity.Button3Click(Sender: TObject);
begin
  if (Edit9.Text = '') or (Edit10.Text = '') or (Edit11.Text = '') then begin

  end else begin
      Edit12.Text := FloatToStr(StrToFloat(Edit9.Text) / (StrToFloat(Edit10.Text) - StrToFloat(Edit11.Text)));
  end;
end;

end.

