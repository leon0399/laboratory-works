unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Grids, ComCtrls, Menus;

type
  TYPE_A = record
    last_name: string[20];
    first_name: string[20];
    school: integer;
    grade: integer;
  end;

  { TForm1 }

  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    Memo1: TMemo;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    StringGrid1: TStringGrid;
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure process(fname, fname_output:string);
var
  f: file of TYPE_A;
  f_output: text;
  rec: TYPE_A;
  grades: array[1..100] of array[1..2] of integer;
  i: integer;
begin
  assign(f, fname);
  reset(f);

  while not eof(f) do begin
    read(f, rec);
    if grades[rec.school, 2] = 0 then begin
      grades[rec.school, 1] := 0;
      grades[rec.school, 2] := 0;
    end;
    inc(grades[rec.school, 1], rec.grade);
    inc(grades[rec.school, 2]);
  end;

  assign(f_output, fname_output);
  rewrite(f_output);

  for i:=1 to 100 do begin
    if grades[i, 2] <> 0 then
      writeln(f_output, i:3, ' ', { round }(grades[i, 1] / grades[i, 2]));
  end;

  close(f);
  close(f_output);
end;

procedure TForm1.MenuItem2Click(Sender: TObject);
begin
  if SaveDialog1.Execute then
    begin
      process(OpenDialog1.FileName, SaveDialog1.FileName);
      Memo1.Lines.LoadFromFile(SaveDialog1.FileName);
    end;
end;

procedure TForm1.MenuItem3Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    begin
      Memo1.Lines.LoadFromFile(OpenDialog1.FileName);
    end;
end;

procedure TForm1.MenuItem5Click(Sender: TObject);
var
  f: file of TYPE_A;
  temp: TYPE_A;
  i: integer;
begin
  if SaveDialog1.Execute then
    begin
      assignfile(f,SaveDialog1.FileName);
      rewrite(f);

      for i:=1 to StringGrid1.RowCount - 1 do
        begin
          temp.last_name := StringGrid1.Cells[1, i];
          temp.first_name := StringGrid1.Cells[2, i];
          temp.school := StrToInt(StringGrid1.Cells[3, i]);
          temp.grade := StrToInt(StringGrid1.Cells[4, i]);

          write(f, temp)
        end;

    closefile(f);
  end;
end;

procedure TForm1.MenuItem6Click(Sender: TObject);
var
  f: file of TYPE_A;
  temp: TYPE_A;
  i: integer;
begin
  if OpenDialog1.Execute then
    begin
      assignfile(f,OpenDialog1.FileName);
      reset(f);
      i := 1;
      while not eof(f) do
       begin
         read(f, temp);
         StringGrid1.RowCount:=i+1;
         StringGrid1.Cells[1, i] := temp.last_name;
         StringGrid1.Cells[2, i] := temp.first_name;
         StringGrid1.Cells[3, i] := IntToStr(temp.school);
         StringGrid1.Cells[4, i] := IntToStr(temp.grade);
         inc(i);
       end;
      closefile(f);
    end;
end;

procedure TForm1.MenuItem8Click(Sender: TObject);
begin
  StringGrid1.RowCount := StringGrid1.RowCount + 1;
end;

procedure DeleteRow(Grid: TStringGrid; ARow: Integer);
var
  i: Integer;
begin
  for i := ARow to Grid.RowCount - 2 do
    Grid.Rows[i].Assign(Grid.Rows[i + 1]);
  Grid.RowCount := Grid.RowCount - 1;
end;

procedure TForm1.MenuItem9Click(Sender: TObject);
begin
  DeleteRow(StringGrid1, StringGrid1.Row);
end;

end.

