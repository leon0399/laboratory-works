unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
  StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    StringGrid1: TStringGrid;
    StringGrid2: TStringGrid;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  arr:array of array of integer;
  R,C:integer;  //R->Row->строки C->Col->столбцы
implementation

{$R *.lfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,j:integer;
begin
R:=StrToInt(Edit1.Text);
C:=StrToInt(Edit2.Text);
StringGrid1.ColCount:=C+1;
StringGrid1.RowCount:=R;
SetLength(arr,R,C+1);
Randomize;
//заполнение массива (и StringGrid) рандомными числами
For i:=0 to R-1 do
For j:=0 to C-1 do
 Begin
  arr[i,j]:=-2+Random(5);//чтобы 0 были почаще
  StringGrid1.Cells[j,i]:=IntToStr(arr[i,j]); //в таблице индексы наоборот
 end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var i,j,k,buf,kol:integer;
begin
// сортировка элементов строк массива (по возрастанию)
StringGrid2.ColCount:=C+1;
StringGrid2.RowCount:=R;
For i:=0 to R-1 do
For j:=0 to C-2 do
For k:=j+1 to C-1 do
If arr[i,j]>arr[i,k] then
 Begin
  buf:=arr[i,j];
  arr[i,j]:=arr[i,k];
  arr[i,k]:=buf;
 end;
//вычисление количества ненулевых элементов в строках
For i:=0 to R-1 do
 Begin
  kol:=0;
  For j:=0 to C-1 do
  If arr[i,j]<>0 then kol:=kol+1;
  arr[i,C]:=kol;
 end;
// помещение счетчика ненулевых элементов в StringGrid(для удобства проверки)
For i:=0 to R-1 do
StringGrid1.Cells[C,i]:=IntToStr(arr[i,C]);
//сортировка строк по количеству ненулевых элементов
For i:=0 to R-2 do
For j:=i+1 to R-1 do
If arr[i,C]>arr[j,C] then
For k:=0 to C do
 Begin
  buf:=arr[i,k];
  arr[i,k]:=arr[j,k];
  arr[j,k]:=buf;
 end;
//заполнение StringGrid2
For i:=0 to R-1 do
For j:=0 to C do
StringGrid2.Cells[j,i]:=IntToStr(arr[i,j]);
end;

end.

