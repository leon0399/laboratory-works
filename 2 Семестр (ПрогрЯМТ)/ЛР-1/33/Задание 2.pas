program laba_2_1_2;

const N = 2;

procedure countIfMore(s: string);
var
  syms: array[0..N] of char; // �������
  nums: array[0..N] of integer; // ������� ������
  i1, i2: integer; // ������� ��� �����
  max, maxI: integer; // ����. ��������
begin
  syms[1]:=s[1];
  syms[2]:=s[length(s)];
  
  for i1:=1 to length(s) do 
    for i2:=1 to N do
      if s[i1] = syms[i2] then nums[i2] := nums[i2] + 1;
      
  maxI:=1;
  max:=nums[maxI];
  for i1:=1 to N do
    if(nums[i1] > max) then
      begin
        max:=nums[i1];
        maxI:=i1;
      end;
      
  println('Symbol: "', syms[maxI], '", count: ', max);
end;

var 
  s:string;
begin
   s:='���������������';//Readln(s);
   countIfMore(s);
end.