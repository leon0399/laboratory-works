program laba_2_1_2;

procedure printNotPresent(s1, s2: string);
var
  c: char;
begin
  for c := 'a' to 'z' do 
    begin
      if (pos(c, s1) = 0) and (pos(c, s2) = 0) then println(c); 
    end;
end;

begin
  printNotPresent('mama myla ramy i upala', 'upala oxhen ochen bolno');
end.