program lab_4;

const 
  FILENAME = 'test.bin';
  LNAMES: array of string = ('�������', '�������', '�������', '��������', '������', '�������', '������', '��������', '�������', '�������');
  FNAMES: array of string = ('������', '����', '���������', '�������', '������', '������', '��������', '�������', '�����', '������');

type
  TYPE_A = record
    last_name: string[20];
    first_name: string[20];
    school: longint;
    grade: longint;
  end;

procedure generate(fname:string);
var
  f: file of TYPE_A;
  rec: TYPE_A;
  len, i: integer;
begin
  randomize;

  assign(f, fname);  
  rewrite(f);
  
  readln(len);
  
  for i:=1 to len do begin
    rec.last_name := LNAMES[random(length(LNAMES))];
    rec.first_name := FNAMES[random(length(FNAMES))];
    rec.grade := random(1, 100);
    rec.school := random(1, 99);
    
    write(f, rec);
  end;
  
  close(f);
end;

procedure write_rec(rec:TYPE_A);
begin
  writeln(rec.last_name:10, ' ', rec.first_name:10, ' ', rec.school:3, ' ', rec.grade:3);
end;

procedure process(fname, fname_output:string);
var
  f: file of TYPE_A;
  f_output: text;
  rec: TYPE_A;
  grades: array[1..100] of array[1..2] of integer;
  i: integer;
begin
  assign(f, fname);  
  reset(f);
  
  while not eof(f) do begin
    read(f, rec);
    write_rec(rec);
    
    inc(grades[rec.school, 1], rec.grade);
    inc(grades[rec.school, 2]);
  end;
  
  writeln;
  writeln('-----------------------------');
  writeln;
  
  assign(f_output, fname_output);
  rewrite(f_output);
  
  for i:=1 to 100 do begin
    if grades[i, 2] <> 0 then
      writeln(i:3, ' ', { round }(grades[i, 1] / grades[i, 2]));
      writeln(f_output, i:3, ' ', { round }(grades[i, 1] / grades[i, 2]));
  end;
  
  close(f);
end;


begin
  generate(FILENAME);
  
  process(FILENAME, 'output.txt');
end.