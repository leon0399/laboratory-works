#include <iostream>
#include <iomanip>
#include <clocale>
#include <cstdlib>
#include <ctime>

std::ostream &setup(std::ostream &stream)
{
	stream << std::setw(10) << std::setprecision(4) << std::setfill('*') << std::right;
	return stream;
}

//
// �������� �������
//
int **Create(size_t n, size_t m) {
	int ** M = new int *[n];
	for (size_t i = 0; i < n; ++i) {
		M[i] = new int[m];
	}
	return M;
}

//
// �������� �������
//
void Free(int **M, size_t n) {
	for (size_t i = 0; i < n; ++i) {
		delete[] M[i];
	}
	delete[] M;
}

//
//---- ���� �������--------------------------------------
//
void Input(int **M, size_t n, size_t m) {
	for (size_t i = 0; i < n; ++i) {
		for (size_t j = 0; j < m; ++j) {
			std::cout << "M[" << i << "][" << j << "] = ";
			std::cin >> M[i][j];
		}
	}
}

//
// ���������� ������� ���������� ������� �� ��������� [0, 99] -----------
//
void FillRandomNumbers(int **matrix, const size_t rows, const size_t columns)
{
	srand((unsigned int)time(0)); // �������������� ����

	for (size_t row = 0; row < rows; row++)
		for (size_t column = 0; column < columns; column++)
			matrix[row][column] = rand() % 100; // ����������� ��
}

//
//-------- ������ ������� ------------------------------------------------
//
void Print(int **M, size_t n, size_t m) {
	for (size_t i = 0; i < n; ++i) {
		for (size_t j = 0; j < m; ++j) {
			std::cout << setup << M[i][j] << ' ';
		}
		std::cout << std::endl;
	}
}

int FindMax(int *M, size_t m) {
	int max = M[0];

	for (size_t i = 0; i < m; ++i)
		if (M[i] > max)
			max = M[i];

	return max;
}

int FindMin(int *M, size_t m) {
	int min = M[0];

	for (size_t i = 0; i < m; ++i)
		if (M[i] < min)
			min = M[i];

	return min;
}

void Process(int **M, int *Res, size_t n, size_t m) {
	int max, min;
	for (size_t i = 0; i < n; ++i) {
		max = FindMax(M[i], n);
		min = FindMin(M[i], n);
		Res[i] = max / min;
	}
}

int main()
{
	setlocale(LC_ALL, "Rus"); // ������� ������ (Windows)

	size_t n, m;

	std::cout << "������� ���������� ����� �������: ";
	std::cin >> n;
	std::cout << "������� ���������� �������� �������: ";
	std::cin >> m;

	int **A = Create(n, m);

	//Input( A, n, m );
	FillRandomNumbers(A, n, m);

	int *S = new int[n]; // ������ ����������
	Process(A, S, n, m);

	// ����� ����������
	for (size_t i = 0; i < n; i++)
		std::cout << S[i] << ' ';
	std::cout << std::endl;

	// ����� �������
	Print(A, n, m);

	// ����������� ������, ���������� ��� ������� � ������
	delete[] S;
	Free(A, n);

	system("pause"); // ��� ������� ������� ����� ������� �� ���������� (windows)

	return 0;
}