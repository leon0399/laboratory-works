#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

template<class T>

class DynArray
{
	struct Node
	{
		T elem;
		Node *next;
		Node(T v)
		{
			elem = v;
			next = 0;
		}
		Node()
		{
			next = 0;
		}
		~Node()
		{
			delete next;
		}

	};

	Node * head;
	int count;

public:
	DynArray()
	{
		head = 0;
		count = 0;
	}
	DynArray(int n)
	{
		head = 0;
		count = 0;
		resize(n);
	}

	DynArray& operator=(const DynArray m)
	{
		resize(0);
		Node * t = m.head;
		while (t != 0)
		{
			Push(t->elem);
			t = t->next;
		}
	}


	DynArray(const DynArray& m)
	{
		head = 0;
		count = 0;
		Node * t = m.head;
		while (t != 0)
		{
			Push(t->elem);
			t = t->next;
		}
	}

	void Push(T v)
	{
		Node *n = new Node(v);
		Node *t = head;
		count++;

		if (t == 0)
		{
			head = n;
			return;
		}

		while (t->next != 0) t = t->next;
		t->next = n;
	}
	void Push()
	{
		Node *n = new Node();
		Node *t = head;
		count++;

		if (t == 0)
		{
			head = n;
			return;
		}

		while (t->next != 0) t = t->next;
		t->next = n;
	}

	void Input(T v, int n)
	{
		if (n >= count) (*this)[n] = v;
		else
		{
			for (int i = count; i > n; i--) (*this)[i] = (*this)[i - 1];
			(*this)[n] = v;
		}
	}

	void Del(int n)
	{
		if (n < count)
		{
			for (int i = n; i < count - 1; i++) (*this)[i] = (*this)[i + 1];
		}
		Pop();
	}

	T Pop()
	{
		Node *t = head;
		if (t->next == 0)
		{
			T res = t->elem;
			delete t;
			head = 0;
			count = 0;
			return res;

		}

		while (t->next->next != 0) t = t->next;

		T res = t->next->elem;
		delete t->next;
		t->next = 0;
		count--;
		return res;

	}

	void Print()
	{
		Node *t = head;
		cout << "[";
		while (t != 0)
		{
			cout << t->elem << ", ";
			t = t->next;
		}
		cout << "]" << count << endl;
	}



	void resize(int n)
	{
		if (n > count)
		{
			while (count < n) Push();
		}
		if (n < count)
		{
			while (count > n) Pop();
		}
	}

	T& operator[](int n)
	{
		if (n >= count)	this->resize(n + 1);

		Node *t = head;
		for (int i = 0; i < n; i++) t = t->next;
		return t->elem;
	}

	friend ostream& operator <<(ostream& out, DynArray m)
	{
		out << "[";
		Node *t = m.head;
		while (t != 0)
		{
			out << t->elem;
			if (t->next != 0) out << ", ";
			t = t->next;
		}
		out << "] " << m.count << endl;
		return out;
	}

	friend istream& operator >> (istream& in, DynArray& m)
	{
		m.resize(0);
		for (int i = 0; in >> m[i]; i++);
		m.Pop();
		return in;
	}



	int countof(int n)
	{
		Node * t = head;
		int k(0);
		while (t != 0)
		{
			if (t->elem == n) k++;
			t = t->next;
		}
		return k;
	}

	friend DynArray operator+(DynArray n, DynArray m)
	{
		DynArray t;

		if (n.count > m.count)
		{
			t.resize(n.count);
			for (int i = 0; i < n.count; i++) t[i] = n[i];
			for (int i = 0; i < m.count; i++) t[i] = t[i] + m[i];
		}
		else
		{
			t.resize(m.count);
			for (int i = 0; i < m.count; i++) t[i] = m[i];
			for (int i = 0; i < n.count; i++) t[i] = t[i] + n[i];
		}

		return t;
	}

	friend int operator*(DynArray n, DynArray m)
	{
		int k;
		int c;

		if (n.count > m.count) c = m.count;
		else c = n.count;

		k = 0;

		for (int i = 0; i < c; i++)
		{
			k += n[i] * m[i];
		}


		return k;
	}

	double abs()
	{
		double k = 0;
		for (int i = 0; i < count; i++)
		{
			k += (*this)[i] * (*this)[i];
		}
		return sqrt(k);
	}

	DynArray operator++()
	{
		for (int i = 0; i < count; i++) ++(*this)[i];
		return (*this);
	}

	DynArray operator++(int)
	{
		for (int i = 0; i < count; i++) (*this)[i]++;
		return (*this);
	}

};