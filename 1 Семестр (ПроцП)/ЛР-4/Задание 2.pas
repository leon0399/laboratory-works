const H=5;
const W=6;

type
  TYPE_ARR=array[1..H,1..W] of real;
  
var
  b: TYPE_ARR;
  
procedure input_arr(var _arr: TYPE_ARR);
var
  i, j: integer;
begin
  for i:=1 to H do 
    for j:=1 to W do
      begin
        // write('b[', i, ',', j, ']=');
        // readln(_arr[i,j]);
        _arr[i,j]:=random(10);
      end;
end;

procedure delete_col(var _arr: TYPE_ARR; k: integer);
var
  i, j: integer;
begin
  for j := k to W-1 do 
    for i := 1 to H do 
      begin
        _arr[i, j] := _arr[i,j+1];
      end;
      
  for i := 1 to H do 
    _arr[i, W] := 0;
end;

procedure process_arr(var _arr: TYPE_ARR);
var
  i, j: integer;
  to_del: array[1..W] of integer;
  counter: integer = 1;
  temp_s, temp_p: real;
begin
  for j:=1 to W do 
    begin
      temp_s:=0;
      temp_p:=1;
      
      for i:=1 to H do
        begin
          temp_s:=temp_s + _arr[i,j];
          temp_p:=temp_p * _arr[i,j]
        end;
      
      if(temp_p > temp_s) then
        begin
          to_del[counter]:=j;
          counter:=counter+1;
        end;
    end;
      
    for i:=1 to counter-1 do 
      begin
        delete_col(_arr, to_del[i] - (i - 1));
      end
end;

procedure print_arr(var _arr: TYPE_ARR);
var
  i, j: integer;
begin
  for i:=1 to H do 
    begin
      for j:=1 to W do
          write(_arr[i,j]:5);
      writeln;
    end;
end;

begin
  input_arr(b);
  
  print_arr(b);
  process_arr(b);
  
  writeln;
  print_arr(b);
end.
