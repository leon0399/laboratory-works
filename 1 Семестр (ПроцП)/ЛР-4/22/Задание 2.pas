const MAX_K=50;
 
type
  TYPE_ARR=array[1..MAX_K, 1..MAX_K] of integer;
 
procedure input_arr(var _arr: TYPE_ARR; m,n: integer);
var
  i, j: integer;
begin
  for i:=1 to m do
    for j:=1 to n do
      begin
        write('a[', i, ',', j, ']=');
        readln(_arr[i,j]);
      end;
end;
 
procedure delete_col(var _arr: TYPE_ARR; l, m, n: integer);
var
  i, j: integer;
begin
  for j := l to n-1 do
    for i := 1 to m do
      begin
        _arr[i, j] := _arr[i,j+1];
      end;
     
  for i := 1 to m do
    _arr[i, n] := 0;
end;
 
procedure delete_str(var _arr: TYPE_ARR; k, m, n: integer);
var
  i, j: integer;
begin
  for i := k to m-1 do
    for j := 1 to n do
      begin
        _arr[i, j] := _arr[i,j+1];
      end;
     
  for j := 1 to m do
    _arr[j, n] := 0;
end;
 
procedure print_arr(var _arr: TYPE_ARR; m, n: integer);
var
  i, j: integer;
begin
  for i:=1 to m do
    begin
      for j:=1 to n do
          write(_arr[i,j]:5);
      writeln;
    end;
end;
 
var
  m: integer = 0;
  n: integer = 0;
  k,l:integer;
  a: TYPE_ARR;
begin
  while not((m<=MAX_K)and(m>0)) do
    begin
      writeln('Введите M');    
      readln(m);
    end;
  while not((n<=MAX_K)and(n>0)) do
    begin
      writeln('Введите M');    
      readln(n);
    end;
   
  input_arr(a, m, n);
 
  print_arr(a, m, n);
 
  while not((k<=m)and(k>0)) do
    begin
      writeln('Введите K');    
      readln(k);
    end;
  while not((l<=n)and(l>0)) do
    begin
      writeln('Введите L');    
      readln(l);
    end;
 
  delete_str(a, k, m, n);
  delete_col(a, l, m, n);
 
  print_arr(a, m, n)
end.