function lab1(a:longint):real;
var
  s,counter:longint;
begin
  while a <> 0 do
    if abs(a) mod 10 <> 0 then
      begin
        if((abs(a) mod 10) mod 2 <> 0) then
          begin
            s:=s + abs(a) mod 10;
            counter:=counter + 1;
          end;
        a:=abs(a) div 10;
      end;
  lab1:=s/counter
end;
 
var
  input: longint;
begin
  writeln('Введите Число');
  readln(input);
  writeln('Среднее арифмитическое нечетных элементов числа=',lab1(input));
end.