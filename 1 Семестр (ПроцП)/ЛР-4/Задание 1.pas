program laba_04_01;

const MAX_K=50;

type
  TYPE_A=array[1..MAX_K] of integer;
  
procedure init(var arr:TYPE_A; var len:integer);
var
  i:longint;
begin
  len := random(MAX_K);
  
  for i:=1 to len do arr[i]:=random(Integer.MaxValue);
end;

procedure output(arr:TYPE_A; len:integer);
var
  i:integer;
begin
  write('before: ');
  for i:=1 to len do write(arr[i], ', ');
end;

procedure reverse(var arr:TYPE_A; len:integer);
var
  buff:TYPE_A;
  i:longint;
begin
  // for i:=len downto 1 do buff[len-i+1]:=arr[i];
  // arr:=buff;
  
  write('after: ');
  for i:=len downto 1 do write(arr[i], ', ');
end;

var
  a:TYPE_A;
  k:integer;
begin
  randomize;
  
  init(a, k);
  output(a, k);
  
  reverse(a, k);
end.
