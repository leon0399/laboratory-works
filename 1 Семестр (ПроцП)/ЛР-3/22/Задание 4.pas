program laba3;
 
var
  A: array[1..5, 1..4] of integer = ((2, 3, 5, -2), (-1, -2, 3, 0), (0, 1, -1, 1), (1, 0, 2, 3), (3, 2, 1, 1));
  i, sum, count: integer;
begin
  for i := 1 to 5 do begin
    if A[i, 2] <> 0 then begin
      sum := sum + A[i, 2];
      count := count + 1;
    end;
  end;
 
  writeln('avg = ', sum / count);
end.