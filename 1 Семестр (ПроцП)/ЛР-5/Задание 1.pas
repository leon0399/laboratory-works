program laba_5_1;
const Ng = 3;
type
  TSport = (skis, skates, hockey, football, rugby, sambo);
  TGroups = array[1..Ng, 1..15] of TSport;
  TLenghts = array[1..Ng] of integer;
  
var
  groups:TGroups;
  lenghts:TLenghts;
  
procedure initGroups(var _groups:TGroups; var _lenghts:TLenghts);
var
  i,j:integer;
begin
  for i:=1 to Ng do
    begin
      _lenghts[i]:=5+random(10);
      for j:=1 to _lenghts[i] do
        begin
          _groups[i][j] := TSport(random(6));
        end;
    end;
end;

procedure printGroups(_groups:TGroups; _lenghts:TLenghts);
var
  i,j:integer;
begin
  for i:=1 to Ng do
    for j:=1 to _lenghts[i] do 
      begin
        writeln(i:4, j:4, ' ', _groups[i][j]);
      end;
end;

procedure checkIfPresent1(_groups:TGroups; _lenghts:TLenghts);
var
  presents:array[0..5] of boolean;
  i,j:integer;
begin
  writeln('Presents at all:');
  for i:=1 to Ng do
    for j:=1 to _lenghts[i] do 
      begin
        if not presents[ord(_groups[i,j])] then
          begin
            presents[ord(_groups[i,j])]:=true;
            writeln('    ', _groups[i,j]);
          end;
      end;
end;

procedure checkIfPresent2(_groups:TGroups; _lenghts:TLenghts);
var
  presents:array[0..5,1..Ng] of boolean;
  i,j:integer;
begin
  writeln('Presents at all groups:');
  for i:=1 to Ng do
    for j:=1 to _lenghts[i] do 
      begin
        if not presents[ord(_groups[i,j]), i] then
          begin
            presents[ord(_groups[i,j]), i]:=true;
          end;
      end;
      
  for i:=0 to 5 do
    if presents[i][1] and presents[i][2] and presents[i][3] then writeln('    ', TSport(i));
end;
  
begin
  randomize;
  
  initGroups(groups, lenghts);
  printGroups(groups, lenghts);
  checkIfPresent1(groups, lenghts);
  checkIfPresent2(groups, lenghts);
end.
