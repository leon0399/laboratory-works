program laba_5_2;

type 
  TTime = record h:0..23; m,s: 0..59 end;

var
  t,t1:TTime;

procedure addSecond(var _t,_t1:TTime);
begin
  _t1:=_t;
  if _t1.s + 1 = 60 then
    begin
      _t1.s:=0;
      
      if _t1.m + 1 = 60 then
        begin
          _t1.m:=0;
          
          if _t1.h + 1 = 24 then 
            begin
              _t1.h:=0;
            end
          else _t1.h:=_t1.h + 1;
          
        end
      else _t1.m:=_t1.m + 1;
      
    end
  else _t1.s:=_t1.s + 1;
end;

begin
  readln(t.h, t.m, t.s);
  println(t.h, ':', t.m, ':', t.s);
  addSecond(t, t1);
  println(t1.h, ':', t1.m, ':', t1.s);
end.